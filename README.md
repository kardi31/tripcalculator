****** What is it? 
Application which calculates the length of trips based on data in the database

****** How to test?
1. Define your database in `.env`
2. Run `bin/console doctrine:mi:mi`
3. Go to `http://127.0.0.1:8000/` 
